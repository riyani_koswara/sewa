-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 26 Jun 2020 pada 04.51
-- Versi server: 5.7.26-0ubuntu0.18.04.1-log
-- Versi PHP: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sewa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_jasa`
--

CREATE TABLE `jenis_jasa` (
  `jenis_jasa_id` int(11) NOT NULL,
  `nama_jasa` varchar(45) DEFAULT NULL,
  `harga_jasa` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_jasa`
--

INSERT INTO `jenis_jasa` (`jenis_jasa_id`, `nama_jasa`, `harga_jasa`) VALUES
(1, 'Riyani Koswara', '300000'),
(2, 'Momon Koswara', '250000'),
(3, 'Suratmi Koswara', '200000'),
(4, 'Ratih Koswara', '300000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `nama_kategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `nama_kategori`) VALUES
(1, 'DCLR'),
(2, 'Miroles'),
(3, 'Kamera Saku'),
(4, 'Pocket');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `kategori_id` varchar(45) DEFAULT NULL,
  `nama_produk` varchar(255) DEFAULT NULL,
  `harga` varchar(45) DEFAULT NULL,
  `stok` varchar(45) DEFAULT NULL,
  `deskripsi` text,
  `gambar` text,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`produk_id`, `kategori_id`, `nama_produk`, `harga`, `stok`, `deskripsi`, `gambar`, `created_on`) VALUES
(1, '2', 'Canon EOS Miroles', '60000', '13', 'Camera Mini dengan lensa yang tajam', '1c4d4fdfd68bc56f3b5d38e0231168b8.jpg', '2020-06-08 05:40:54'),
(3, '3', 'Sony Cybershoot', '30000', '15', 'Kamera mdah dibawa kemanapun dengan hasil yan jernih', '5f2ef0c3e4b45ba5d462fdd43712246d.jpg', '2020-06-08 06:03:35'),
(4, '4', 'Canon Ixus', '30000', '10', 'Camera bentuk mini dengan kualitas foto yang bagus', '9654c5eb5f4d9306bee2e793f230740b.jpg', '2020-06-08 06:09:40'),
(5, '1', 'Canon EDS 70', '120000', '6', 'Lensa Panjang sehingga memudahkan untuk memfokuskan benda yang akan difoto', 'c0b50c8eb842b373982aad3a76e26945.jpg', '2020-06-08 06:15:57'),
(7, '4', 'Canon sx740', '80000', '22', 'Selain foto kamera ini juga bisa digunakan untuk merekam video', 'd22fcd4ca0e36ae52be391e352c21cdc.jpg', '2020-06-08 06:24:55'),
(8, '4', 'Fuji-XF10', '50000', '13', 'Kamera mini ringan mudah dibawa kemanapun', '439a7685ef3f88a650fb9c48ee92e2b2.jpg', '2020-06-08 06:29:58'),
(9, '4', 'Sony RX-100', '50000', '21', 'Kamera mini yang mudah dibawa kemanapun', '8fb9cd08268540f808d135050e83c241.jpg', '2020-06-08 06:31:56'),
(10, '3', 'X-AS', '50000', '20', 'Kamera mini dengan kualitas bagus', 'cc1dfbc0c56cde9d6d78c6f67d89fa2d.jpg', '2020-06-08 06:41:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `transaksi_id` int(11) NOT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `produk_id` varchar(45) DEFAULT NULL,
  `dari` date DEFAULT NULL,
  `sampai` date DEFAULT NULL,
  `jumlah` varbinary(45) DEFAULT NULL,
  `harga` varchar(45) DEFAULT NULL,
  `status` enum('1','0','2') DEFAULT '0' COMMENT '2 kembali',
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_jasa`
--

CREATE TABLE `transaksi_jasa` (
  `transaksi_jasa_id` int(11) NOT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `keterangan` text,
  `jenis_jasa_id` varchar(45) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `status` enum('1','0') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_jasa`
--

INSERT INTO `transaksi_jasa` (`transaksi_jasa_id`, `user_id`, `keterangan`, `jenis_jasa_id`, `tanggal_mulai`, `created_on`, `status`) VALUES
(1, '2', 'Foto Prewedding di Jalan Batang jam Sore', '1', '2020-06-03', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `nama_lengkap` varchar(60) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `notelp` varchar(20) DEFAULT NULL,
  `level` enum('0','1') DEFAULT '1',
  `blokir` varchar(1) DEFAULT '0',
  `alamat` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `password`, `nama_lengkap`, `email`, `notelp`, `level`, `blokir`, `alamat`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '087764084641', '0', '0', 'Badakarya\r\n'),
(2, 'sikos', 'Riyani Koswara', 'riyanikoswara8@gmail.com', '087764084641', '1', '0', 'Wanadadi'),
(3, 'enca', 'Enca', 'enca@gmail.com', '087764531241', '0', '0', 'Punggelan'),
(5, 'si', 'Enca', 'riyanikoswara@gmail.com', '087764084641', '0', '0', 'Wanadadi');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jenis_jasa`
--
ALTER TABLE `jenis_jasa`
  ADD PRIMARY KEY (`jenis_jasa_id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`transaksi_id`);

--
-- Indeks untuk tabel `transaksi_jasa`
--
ALTER TABLE `transaksi_jasa`
  ADD PRIMARY KEY (`transaksi_jasa_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jenis_jasa`
--
ALTER TABLE `jenis_jasa`
  MODIFY `jenis_jasa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `transaksi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaksi_jasa`
--
ALTER TABLE `transaksi_jasa`
  MODIFY `transaksi_jasa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
