<?php $this->load->view('header'); ?>
<style media="screen">
.reg{
  margin-left: 400px;
  padding: 80px 0 20px 0;
  background-color: #eaeaea;
  border-radius: 5px;
  text-align: center;
  width: 400px;
  height: 650px;
  margin-top: -30px;
}
input.user {
    width: 80%;
    background: none;
    border: none;
    border-bottom: 1px solid white;
    color: white;
    padding: 20px;
    font-size: 18px;
    font-family: ClementePDac-ExtraLight;
    outline: none;
}
h2{
  text-align: center;
    color: #C64444;
    margin-top: -40px;
    font-family: ClementePDai-Regular;
}
</style>
<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard') ?>">Home</a></li>
            <li><a href="<?php echo base_url('login') ?>">LOGIN</a></li>
            <li class="<?php echo base_url('daftar') ?>">REGISTER</li>
        </ol>
    </div>
</div>
<div class="reg-form">
  <div class="container">
    <div class="reg">
      <h2>Daftar RenCam</h2>
      <p>Selamat datang, silahkan melakukan pendaftaran.<p>
      <p>Jika anda sudah punya akun, selahkan <a href="<?php echo base_url('login')?>">Klik Disini</a></p>
      <?php echo $this->session->flashdata('msg');?>
       <form data-parsley-validate  action="<?php echo base_url('daftar/simpan')?>" method="post">
          <input type="nama" name="nama_lengkap" class="user" placeholder="Username "  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'nama_lengkap';}" />
          <?php echo form_error('nama_lengkap','<div class="text-danger small ml-2 mb-3">', '</div>' ) ?>
           <input name="notelp" type="notelp" class="user"  placeholder="No Telepon" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'notelp';}" />
           <?php echo form_error('notelp','<div class="text-danger small ml-2 mb-3">', '</div>' ) ?>
           <input type="nama" name="alamat" class="user" placeholder="Alamat"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'alamat';}" />
           <?php echo form_error('alamat','<div class="text-danger small ml-2 mb-3">', '</div>' ) ?>
           <input name="email" type="email" class="user"  placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'email';}" />
           <?php echo form_error('email','<div class="text-danger small ml-2 mb-3">', '</div>' ) ?>
            <input type="password" class="user" id="exampleInputpasswoard" placeholder="Enter Password" name="password_1" />
                <?php echo form_error('password_1','<div class="text-danger small ml-2 mb-3">', '</div>' ) ?>
                <input type="password" class="user" id="exampleRepeatPasswoard" placeholder="Repeat Password" name="password_2" />
                <br>
          <input type="submit" value="Daftar">
      </form>
    </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('footer'); ?>
