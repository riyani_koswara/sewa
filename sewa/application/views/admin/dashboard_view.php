<?php $this->load->view('admin/header') ?>
<div class="container-fluid">
          <div class="row">
            <div class="col-xl-3 col-md-5 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-3"><a href="<?php echo base_url('data_produk') ?>">Data Produk </a>
                      </div>
                      <div class="h5 mb-0 font-weight-bold text-black-800"><h1><?php echo $produk?></h1>
                      </div>
                    </div>
                      <i class="fas fa-box fa-5x text-black-300"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-5 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-3"><a href="<?php echo base_url('data_transaksi_sewa') ?>">Data Transaksi Sewa </a>
                      </div>
                      <div class="h5 mb-0 font-weight-bold text-black-800"><h1><?php echo $sewa?></h1>
                      </div>
                    </div>
                      <i class="fas fa-money fa-5x text-black-300"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-5 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-3"><a href="<?php echo base_url('data_transaksi_jasa') ?>">Data Transaksi Jasa </a>
                      </div>
                      <div class="h5 mb-0 font-weight-bold text-black-800"><h1><?php echo $jasa?></h1>
                      </div>
                    </div>
                      <i class="fas fa-box fa-5x text-black-300"></i>
                  </div>
                </div>
              </div>
            </div>

             <div class="col-xl-3 col-md-5 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-3"><a href="<?php echo base_url('data_member') ?>">Data Member </a>
                      </div>
                      <div class="h5 mb-0 font-weight-bold text-black-800"><h1><?php echo $member?></h1>
                      </div>
                    </div>
                      <i class="fas fa-users fa-5x text-black-300"></i>
                  </div>
                </div>
              </div>
            </div>

</div>

<?php $this->load->view('admin/footer') ?>
