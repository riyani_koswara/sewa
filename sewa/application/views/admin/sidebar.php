<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="list-unstyled components">
                <div class="foto">
                  <center>
                <img src="<?php echo base_url('theme/admin/img/sikos.jpg') ?>" height = "100px" width="100px" class="img-circle">
                <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo $this->session->userdata('nama');?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo base_url('login/logout')?>" class="btn btn-default btn-sm"><i class="fas fa-close"></i> Log Out</a>
                </div>
              </li>
              </center>
            </div>
            <hr color="white" />
      <ul class="sidebar-menu" data-widget="tree">
        <li><a href="<?php echo base_url('dashboard')?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <li><a href="<?php echo base_url('data_produk')?>"><i class="fa fa-box"></i> <span>Produk</span></a></li>
        <li><a href="<?php echo base_url('data_transaksi_sewa')?>"><i class="fa fa-money"></i> <span>Transaksi Sewa</span></a></li>
        <li><a href="<?php echo base_url('data_transaksi_jasa')?>"><i class="fa fa-money"></i> <span>Transaksi Jasa</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('data_kategori_sewa')?>"><i class="fa fa-circle"></i> Kategori Produk</a></li>
            <li><a href="<?php echo base_url('data_kategori_jasa')?>"><i class="fa fa-circle"></i> Kategori Jasa</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('laporan_sewa')?>"><i class="fa fa-file"></i> Transaksi Sewa</a></li>
            <li><a href="<?php echo base_url('laporan_jasa')?>"><i class="fa fa-file"></i> Transaksi Jasa</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url('data_member')?>"><i class="fa fa-users"></i> <span>Manage Member</span></a></li>
      </ul>
    </section>
  </aside>
