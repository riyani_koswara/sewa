<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Rental Camera</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() {setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<meta charset utf="8">
	    <link href="<?php echo base_url('theme/user/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('theme/user/css/style.css');?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('theme/admin/fontawesome-free/css/all.min.css')?>">
    <script src="<?php echo base_url('theme/user/js/simpleCart.min.js');?>"></script>
		<script src="<?php echo base_url('theme/user/js/jquery-2.1.4.min.js');?>"></script>
		<script src="<?php echo base_url('theme/user/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('theme/user/js/imagezoom.js');?>"></script>
    <script defer src="<?php echo base_url('theme/user/js/jquery.flexslider.js');?>"></script>
    <script defer src="<?php echo base_url('theme/user/js/parsley.min.js');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('theme/user/css/flexslider.css');?>" type="text/css" media="screen" />
    <style>
    body { /* Body offset for fixed navbar */
    padding-top: 70px;
    }
        #main-navbar .nav-link{
      border-bottom: 5px solid #f9f9f9;
      color: white;
    }
.bubu {
    color: #191970;
    font-size: 20px;
    font-family: Arial Black;
    margin-left: 820px;
    margin-bottom: -30px;
}
.img-cir{
  border-radius: 50%;
  width: 200px;
  height: 200px;
}
    #main-navbar .nav-link:hover,  #main-navbar a.active{
      border-bottom: 5px solid #f94c27;
      color: #999999;
    }
    .navbar-default {
        background-color: #191970;
        border-color: #191970;
    }

    .logo a, .logo a:hover {
        font-family: 'Pathway Gothic One';
        background-color: #191970;
        text-transform: uppercase;
        text-decoration: none;
         padding: 0 60px;
        font-weight: bold;
        font-size: 34px;
        color: #fff;
    }

    .iki{
      margin-top: 50px;
    }
    .text-white {
    color: #fff !important;
}

    .navbar .navbar-form { /* Positioning the form */
     border-bottom: 5px solid #f94c27;
    color: #999999;
    }
    .tata{
      width: 600px;
      height: 500px;
    }


    @media(max-width:767px) {
      body { /* Body offset for fixed navbar for mobile devices */
      padding-top: 140px;
    }

    .navbar .navbar-form {
          width: 100% /* Full width search box for mobile devices */
      }
    }


    /* Media queries to adjust form width */
    @media(min-width:768px) {
      .navbar-form .input-group>.form-control {
      width: 200px;
    }
    }

    @media(min-width:992px) {
    .navbar-form .input-group>.form-control {
      width: 270px;
    }
    }

    @media(min-width:1200px) {
    .navbar-form .input-group>.form-control {
      width: 370px;
    }
    }
    .navbar-default .navbar-nav>li>a {
        color: #fff;
    }
    .footer{
      background-color: #191970;
      width: 100%;
      height: 200px;
    }
    </style>
    </head>
    <body>

      <div class="header">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".upper-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <div class="logo">
                      <a href="<?php echo base_url();?>"><i class="fas fa-camera-retro"></i> RenCam</a>
                  </div>
                </div>
                <nav id="main-navbar" class="navbar navbar-expand-md navbar-light bg-light fixed-top py-2 py-md-0">
                <div class="collapse navbar-collapse upper-navbar">
                  <ul class="nav navbar-nav ">
                    <li class="nav-item">
                <a class="nav-link p-4 active" href="<?php echo base_url('home');?>">Home</a>
                      </li>
                    <li class="nav-item">
                 <a class="nav-link p-4" href="<?php echo base_url('produk');?>">Sewa</a>
                   </li>
                <li class="nav-item">
                 <a class="nav-link p-4" href="<?php echo base_url('jasa');?>">Jasa</a>
                  </li>
                  </ul>
                  <ul id="menu-topmenu" class="nav navbar-nav navbar-right">
                    <?php if (empty($this->session->userdata('is_login'))){ ?>
                      <li><a href="<?php echo base_url('daftar')?>" role="button">Daftar</a></li>
                      <li><a href="<?php echo base_url('login')?>" role="button">Masuk</a></li>
                    <?php }else{ ?>
                      <li><a href="<?php echo base_url('akun')?>" role="button"><?php echo ucfirst($this->session->userdata('nama'));?></a></li>
                      <li><a href="<?php echo base_url('login/logout')?>" role="button">Logout</a></li>
                    <?php } ?>
                  </ul>
                </div>
          </div>
        </nav>
      </div>
