<?php $this->load->view('header') ?>

<style media="screen">
.log{
  margin-left: 400px;
  padding: 80px 0 20px 0;
  background-color: #eaeaea;
  border-radius: 5px;
  text-align: center;
  width: 300px;
  height: 450px;
  margin-top: -30px;
}
input.user {
    width: 80%;
    background: none;
    border: none;
    border-bottom: 1px solid white;
    color: white;
    padding: 20px;
    font-size: 18px;
    font-family: ClementePDac-ExtraLight;
    outline: none;
}
h2{
  text-align: center;
    color: #C64444;
    margin-top: -40px;
    font-family: ClementePDai-Regular;
}
</style>
<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home') ?>">HOME</a></li>
            <li><a href="<?php echo base_url('login') ?>">LOGIN</a></li>
        </ol>
    </div>
</div>
<div class="login">
    <div class="container">
        <div class="log">
                     <h2>Login</h2>
                     <p>Selamat Datang, Silahkan masuk</p>
                     <p>Jika belum punya akun, silahkan <a href="<?php echo base_url('daftar')?>">klik disini</a></p>
                <?php echo $this->session->flashdata('message'); ?>
            <form action="<?php echo base_url('login/aksi_login')?>" method="post">
            <input name="email" type="text" class="user"  placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" />
                      <?php echo form_error('email', '<div class="text-danger small ml-2">',
                                            '</div>'); ?>
          <input type="password" name="password" class="user" placeholder="Password "  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" />
                    <?php echo form_error('password', '<div class="text-danger small ml-2">',
                                            '</div>'); ?><br>
            <input type="submit" value="Login">
                     </form>
            </div>
        </div>
    </div>
<br>
<?php $this->load->view('footer') ?>
