  <!-- Contact Section -->
    <div class="container">
      <div class="row">
        <div class="footer">

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-map-marked-alt text-white mb-2"></i>
              <h4 class="text-uppercase text-white m-0">Address</h4>
              <hr class="my-4">
              <div class="small text-white">Badakarya, Gamblok</div>
            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-envelope text-white mb-2"></i>
              <h4 class="text-uppercase text-white m-0">Email</h4>
              <hr class="my-4">
              <div class="small text-white">
                <a href="#" class="text-white">riyanikoswara8@gmail.com</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-mobile-alt text-white mb-2"></i>
              <h4 class="text-uppercase text-white m-0">Phone</h4>
              <hr class="my-4">
              <div class="small text-white">087764084641</div>
            </div>
          </div>
        </div>

        <center>
      <div class="social d-flex justify-content-center">
        <a href="#" class="mx-2">
          <i class="fab fa-twitter text-white"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-facebook-f text-white"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-github text-white"></i>
        </a>
    </center>
  <!-- Footer -->
  <footer class="iki small text-center text-white foot">
      Copyright &copy; RenCam2020
  </footer>
</div>
</center>
</div>
</div>
</body>
</html>
