<?php

/**
 *
 */
class Daftar extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    $this->load->view('daftar_view');
  }

  public function simpan()  {
    $this->form_validation->set_rules('nama_lengkap','nama_lengkap','required');
    $this->form_validation->set_rules('notelp','notelp','required');
    $this->form_validation->set_rules('alamat','alamat','required');
    $this->form_validation->set_rules('email','email','required|valid_email|is_unique[users.email]');
    $this->form_validation->set_rules('password_1','password','required|matches[password_2]');
    $this->form_validation->set_rules('password_2','password','required|matches[password_1]');
    if ($this->form_validation->run() == FALSE){
    $this->load->view('daftar_view');
  }else {
    $data =array(
      'user_id' => '',
      'nama_lengkap' => $this->input->post('nama_lengkap'),
      'email' => $this->input->post('email'),
      'notelp' => $this->input->post('notelp'),
      'alamat' => $this->input->post('alamat'),
      'password' => $this->input->post('password_1'),
      'level' => 1
    );
    $this->db->insert('users', $data);
    redirect('login/index');
  }
  }
}

 ?>
