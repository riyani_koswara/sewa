<?php 
	class Model_auth extends CI_Model{
		public function cek_login(){
			$nama_lengkap = set_value('nama_lengkap');
			$password = set_value('password');

			$result = $this->db->where('nama_lengkap', $nama_lengkap)
							   ->where('password', $password)
							   ->limit(1)
							   ->get('users');
			if($result->num_rows() > 0 ){
				return $result->row();
			}else{
				return array();
			}
		}
	}
 ?>